import { conn } from "../src/connection";
import { ClientDaoPostgres } from "../src/daos/bank-client-dao-imple";
import { BankDao } from "../src/daos/bank-dao";
import { Account, BankClient } from "../src/entities";

const bankDao:BankDao = new ClientDaoPostgres

test("Create the first client", async()=>{
    const testClient:BankClient = new BankClient(0,"Donavan","Merrin");

    const bankClient:BankClient = await bankDao.createClient(testClient)
    expect(bankClient).not.toBe(0);

});

// test("Check your account balance",async()=>{
//      const testAccount:Account = new Account(0,10,0,0);
//      const account:Account = await bankDao.getAccountBalance(testAccount);
//      expect(account.amount).toBe(10);
 //}); 
test("Get all bank clients ",async()=>{
   let bankClient1:BankClient = new BankClient (0,'Donavan','Merrin')
   let bankClient2:BankClient = new BankClient (0,'Sandra','Musszynski')
   let bankClient3:BankClient = new BankClient (0,'Ash','Naik')
   let bankClient4:BankClient = new BankClient (0,'Charles','Jester')
   await bankDao.createClient(bankClient1)
   await bankDao.createClient(bankClient2)
   await bankDao.createClient(bankClient3)
   await bankDao.createClient(bankClient4)

   const bankClient:BankClient[] = await bankDao.getAllClients();
   expect (bankClient.length).toBeGreaterThanOrEqual(4)
});

test("Get account by ID",async () => {
let account1:Account = new Account (0,10,0,1)
let account2:Account = new Account (0,10,0,2)
let account3:Account = new Account (0,10,0,3)
let account4:Account = new Account (0,10,0,4)
await bankDao.createAccount(account1,1)
await bankDao.createAccount(account2,2)
await bankDao.createAccount(account3,3)
await bankDao.createAccount(account4,4)

const account:Account[] = await bankDao.getAllAccounts();
expect (account.length).toBeGreaterThanOrEqual(4)
})
test("create account",async () => {
    let account1:Account = new Account (0,20,0,1)
    let account2:Account = new Account (0,20,0,1)

    await bankDao.createAccount(account1,1)
    await bankDao.createAccount(account2,1)

    const account:Account[] = await bankDao.getAllAccounts();
    expect (account.length).toBeGreaterThanOrEqual(2)
})

test("Get bank client by ID",async () => {
    let bankClient:BankClient = new BankClient(0,'James','Ran')
    bankClient = await bankDao.createClient(bankClient)
    bankClient.bankClientId
    let getBankClient:BankClient = await bankDao.getClientById(bankClient.bankClientId)

    expect(getBankClient.fname).toBe(bankClient.fname)
})
test("update bank client", async()=>{
    let bankClient:BankClient = new BankClient(1,'Donny','Merrin')
    bankClient = await bankDao.createClient(bankClient)

    bankClient.lname = 'Meow';
    bankClient = await bankDao.updateClient(bankClient)
    expect (bankClient.lname).toBe('Meow')
})

test("update account", async()=>{
    let account:Account = new Account(0,0,0,1)
    account = await bankDao.createAccount(account,1)

    account.acctType = 1;
    account = await bankDao.updateAccount(account)
    expect(account.acctType).toBe(1)
})

test("delete client by id",async()=>{
    let bankClient:BankClient = new BankClient(0,'Charles','J');
    bankClient = await bankDao.createClient(bankClient);

    const result:boolean = await bankDao.deleteClientById(bankClient.bankClientId);
    expect(result).toBeTruthy();
})

test("delete accounts",async()=>{
    let account:Account = new Account(0,1000,0,7)
    let account2:Account = await bankDao.createAccount(account,7)

    const result:boolean = await bankDao.deleteAccountById(account2.accountId)
    expect(result).toBeTruthy();

})

test("deposit",async () => {
    let account:Account = new Account(9,1000,0,6)
    let account1:Account = await bankDao.getAccountById(account.accountId)

    let account2:Account = await bankDao.modifyAccount(account,20,0);
     expect(account.amount).toBe(account2.amount)
 })
test("withdraw", async()=>{
    let account:Account = new Account(9,1000,0,6)
    let account1:Account = await bankDao.getAccountById(account.accountId);
    account = await bankDao.modifyAccount(account1,20,1)
    expect(account.amount).toBe(account1.amount)
})

afterAll(async()=>{
    conn.end()
})