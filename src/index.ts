import express from "express";
import { Account, BankClient } from "./entities";
import { MissingResourceError } from "./errors";
import ClientAccountServices from "../services/client-account-services";
import { ClientAccountServiceImpl } from "../services/client-account-services-imple";


const app = express();
app.use(express.json())

const clientAccountServices:ClientAccountServices = new ClientAccountServiceImpl

app.get("/clients",async(req,res)=>{
    try{const result:BankClient[] = await clientAccountServices.retrieveAllClients();
    res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
        }
});//gets all clients

app.get("/clients/:id",async(req,res)=>{
    try{
        const clientId:number = Number(req.params.id)
    const accounts:BankClient = await clientAccountServices.retrieveClientById(clientId)
    res.send(accounts)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
});//get client by id
app.get("/clients/:id/accounts", async(req,res)=>{
    try{const bank_Client_Id:number = Number(req.params.id)
    const result:Account[] = await clientAccountServices.retrieveClientAccounts(bank_Client_Id) 
    res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
});// gets all accounts for client 7 return 404 if no exist

app.get("/accounts/:id",async(req,res)=>{
    const account_id = Number(req.params.id);
    const account:Account = await clientAccountServices.retrieveAccountById(account_id)
    res.send(account)
});//get account with id 4 return 404 if no account or client exists

app.post("/clients", async(req,res)=>{
    const bankClient:BankClient = req.body
    const result:BankClient = await clientAccountServices.createNewClient(bankClient)
    res.status(201)
    res.send(result)
});// creates a new client return a 201 code

app.post("/accounts/:id",async(req,res)=>{
    const account:Account = req.body;
    const bankClientId:number = Number(req.params.id)
    const result:Account = await clientAccountServices.createNewAccountByClientId(account,bankClientId,0)
    res.send(result)
});// creates a new client return 201 status code

app.put("/accounts/:id", async (req,res)=>{
    try{const account_id:number = Number(req.params.id);
    const account:Account = req.body;
    const testAccountID:Account = await clientAccountServices.retrieveAccountById(account_id)
    const testClientID:BankClient = await clientAccountServices.retrieveClientById(account.bankClientId)

    if(testClientID.bankClientId != account.bankClientId){
        throw Error("Client ID's do not match")
        
    }else if(testAccountID.accountId != account_id){
        await clientAccountServices.createNewAccount(account, account.bankClientId)
    }else{
        await clientAccountServices.updateAccountById(account)
    }
    res.send(account);
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
    }
});//updates client with with id of 12 return 404 if no such client exist

app.put("/clients/:id",async(req,res)=>{
    try{
        const client_id:number = Number(req.params.id)
    const bankClient:BankClient = req.body;

    const updatedClient = await clientAccountServices.updateClientById(bankClient);

    res.send(updatedClient)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)        
    }
});// update account with the id 3 return 404 if no exists

app.patch("/accounts/:id/deposit",async(req,res)=>{
    try{ const account_id:number = Number(req.params.id);
    const deposit:number = req.body.amount;

    const result:Account = await clientAccountServices.depositAccount(account_id,deposit,0)
    res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(404)
        res.send(error)
        }
});//12 deposit given amount (Body{"amount:500"}) return 404 if no exists

app.patch("/accounts/:id/withdraw",async(req,res)=>{
    try{const account_id:number = Number(req.params.id);
    const withdraw:number = req.body.amount;

    const result:Account = await clientAccountServices.withdrawAccount(account_id,withdraw,1)

    res.send(result)
    }catch(error){
        if(error instanceof MissingResourceError)
        res.status(422)
        res.send(error)
    }
});//withdraw deposit given amount (Body{"amount":500}) return 404 if no account exists return 422 if insufficient funds

app.delete("/clients/:id",async(req,res)=>{
    try {
    const bank_client_id = Number(req.params.id)
    await clientAccountServices.deleteClientById(bank_client_id)
    res.status(205)
    res.send(`The Client with ID ${bank_client_id} has been deleted.`)
    } catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error);
        }
    }
});//deletes client with id. return 404 if no such client exist
    

app.delete("/accounts/:id", async(req,res)=>{
    try{
        const account_id = Number(req.params.id)
        await clientAccountServices.deleteAccountById(account_id)
        res.status(205);
        res.send(`The Account with ID ${account_id} has been deleted.`)
    }catch(error){
        if(error instanceof MissingResourceError){
            res.status(404)
            res.send(error)   
            }
            
    }
});//delete account 6 return 404 if no exists

app.listen(3000,()=>{console.log("Application Started")})