
export class BankClient{
    constructor(
        public bankClientId: number,
        public fname: string,
        public lname:string
    ){}

}
export class Account{
    constructor(
        public accountId:number,
        public amount:number,
        public acctType:number,
        public bankClientId:number,
        ){}    
}