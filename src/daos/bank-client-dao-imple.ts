import { BankClient, Account } from "../entities";
import { BankDao } from "./bank-dao";
import { conn } from "../connection"
import { MissingResourceError } from "../errors";


export class ClientDaoPostgres implements BankDao{
       
    async createAccount(account:Account, bank_Client_Id: number): Promise<Account> {
        const sqltest = "select bank_client_id from bankclient where bank_client_id = $1"
        const valtest = [bank_Client_Id];
        const resttest = await conn.query(sqltest, valtest);
        
        //const testing = resttest.rows[0] ?? 0;

        // if(testing === 0){
        // const sql:string = "insert into bankclient(fname,lname) values ($1,$2) returning client_id"
        // const values = [bankClient.fname,bankClient.lname];
        // const result = await conn.query(sql,values);
        // bankClient.bankClientId = result.rows[0].client_id;
        // }
        
        const sql2:string = "insert into account(account_balance,account_type) values ($1,$2) returning account_id,c_id";
        const values2 = [account.amount,account.acctType];
        const result2 = await conn.query(sql2,values2);
        account.accountId = result2.rows[0].account_id;
        console.log(account)
        return account;
    }
    async createClient(bankClient: BankClient): Promise<BankClient> {
        const sql:string = "insert into bankclient(fname,lname) values ($1,$2) returning bank_client_id"
        const values = [bankClient.fname,bankClient.lname];
        const result = await conn.query(sql,values);
        bankClient.bankClientId = result.rows[0].bank_client_id;
            
        return bankClient;
    }
    async getAllClients(): Promise<BankClient[]> {
        const sql:string = 'select * from bankclient';
        const result = await conn.query(sql);
        const bankClients:BankClient[] = [];
        for(const row of result.rows){
            const bankClient:BankClient = new BankClient(
                row.bank_client_id,
                row.fname,
                row.lname);
            bankClients.push(bankClient);
            }
            return bankClients;
    }
    async getClientById(bankClientId: number): Promise<BankClient> {
        const sql:string = 'select * from bankclient where bank_client_id = $1';
        const values = [bankClientId]
        const result = await conn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The client with ${bankClientId} does not exist`)
        }
        const row = result.rows[0];
        const bankClient:BankClient = new BankClient(
            row.bank_client_id,
            row.fname,
            row.lname);
            return bankClient;
    }
    async getAllAccounts(): Promise<Account[]> {
        const sql:string = "select * from account";
        const result = await conn.query(sql);
        const accounts:Account[]=[];
        for(const row of result.rows){
            const account:Account = new Account(
                row.c_id,
                row.account_balance,
                row.account_type,
                row.account_id
            );
            accounts.push(account)
        }
            return accounts
    }
    async getAccountById(accountId: number): Promise<Account> {
        const sql:string = 'select * from account where account_id = $1';
        const values = [accountId]
        const result = await conn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`The account with ${accountId} does not exist`)
        }
        const row = result.rows[0];
        const account:Account = new Account(
            row.account_id,
            row.account_balance,
            row.account_type,
            row.c_id);
        return account;
    }
    async getAccountsByClientId(bank_client_id: number): Promise<Account[]> {
        const sql:string = 'select * from account where c_id = $1';
        const values = [bank_client_id]
        const result = await conn.query(sql,values);
        const accounts:Account[] = [];
        for(const row of result.rows){
            const account:Account = new Account(
            row.account_id,
            row.account_balance,
            row.account_type,
            row.c_id
        );
            accounts.push(account);
    }    
        return accounts;
    }
/*     async getAccountBalance(account:Account): Promise<Account> {
        const sql:string = 'select account_balance from account where account_balance = $2';
        const values = [account.amount]
        const result = await conn.query(sql,values)
        const 
    } */
    async updateClient(bankClient: BankClient): Promise<BankClient> {
        const sql:string = 'update bankclient set fname =$1, lname = $2 where bank_client_id = $3';
        const values = [bankClient.fname,bankClient.lname,bankClient.bankClientId];
        const result = await conn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`the client with id ${bankClient.bankClientId} does not exist`)
        }
        return bankClient;
    }
    async updateAccount(account:Account): Promise<Account> {
        const sql:string = 'update account set account_balance = $1, account_type = $2 where c_id = $3';
        const values = [account.amount,account.acctType,account.bankClientId];
        const result = await conn.query(sql,values);
        // if(result.rowCount === 0){
        //     throw new MissingResourceError(`the account with id ${account.accountId} does not exist`)
        //}
        return account;
    }
    async modifyAccount(account:Account,amount:number,transactionType:number):Promise<Account>{
        if(transactionType === 0){
            const sql:string = 'update account set account_balance = account_balance + $1 where account_id = $2 returning account_balance'
            const values = [amount,account.accountId]
            const result = await conn.query(sql,values);
            account.amount = result.rows[0].account_balance
            }else{
        const sql:string = 'update account set account_balance = account_balance - $1 where account_id = $2 returning account_balance'
        const values = [amount,account.accountId]
        const result = await conn.query(sql,values);
        account.amount = result.rows[0].account_balance    
        } 
        
        return account;
    }
    async deleteClientById(bankClientId: number): Promise<boolean> {
        const sql1:string = 'delete from account where c_id=$1'
        const val2 = [bankClientId]
        const result2 = await conn.query(sql1,val2)

        const sql:string = 'delete from bankclient where bank_client_id=$1';
        const values = [bankClientId];
        const result = await conn.query(sql,values);
        if(result.rowCount === 0){
            throw new MissingResourceError(`the client with id ${bankClientId} does not exist`)
        }
        return true;
    }
    async deleteAccountById(accountId: number): Promise<boolean> {
        const sql:string = 'delete from account where account_id = $1';
        const values = [accountId];
        const result = await conn.query(sql,values);  
        return true;;
    }
    
}