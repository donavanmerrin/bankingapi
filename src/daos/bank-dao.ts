import { Account, BankClient } from "../entities";


export interface BankDao{
    //Create
    createAccount(account:Account,bankClientId:number):Promise<Account>;
    createClient(bankClient:BankClient):Promise<BankClient>;
    //Read
    getAllClients():Promise<BankClient[]>;
    getClientById(bank_Client_Id:number):Promise<BankClient>;
    getAllAccounts():Promise<Account[]>;
    getAccountById(account:number):Promise<Account>;
    getAccountsByClientId(bank_Client_Id:number):Promise<Account[]>;
    //getAccountBalance(account:Account):Promise<Account>;
    //Update
    updateClient(bank_Client_Id:BankClient):Promise<BankClient>;
    updateAccount(account:Account):Promise<Account>;
    //Delete
    modifyAccount(account:Account,amount:number,transactionType:number):Promise<Account>
    deleteClientById(bank_Client_Id:number):Promise<boolean>;
    deleteAccountById(account_id:number):Promise<boolean>;
}