import { Account, BankClient } from "../src/entities";
import ClientAccountServices from "./client-account-services";
import { BankDao } from "../src/daos/bank-dao";
import { ClientDaoPostgres } from "../src/daos/bank-client-dao-imple";
import { MissingResourceError } from "../src/errors";
export class ClientAccountServiceImpl implements ClientAccountServices{
    bankDao:BankDao = new ClientDaoPostgres()
    createNewAccount(account: Account,acctType:number): Promise<Account> {
        account.acctType = acctType;
        const bank_Client_Id = account.bankClientId
        return this.bankDao.createAccount(account,bank_Client_Id)
    }
    createNewClient(bankclient: BankClient): Promise<BankClient> {
        return this.bankDao.createClient(bankclient)
    }
    createNewAccountByClientId(account: Account,bankClientId:number,acctType:number): Promise<Account> {
        return this.bankDao.createAccount(account,0)
    }
    retrieveAllClients(): Promise<BankClient[]> {
        return this.bankDao.getAllClients()
    }
    retrieveClientById(bank_client_id: number): Promise<BankClient> {
        return this.bankDao.getClientById(bank_client_id)
    }
    retrieveAccountById(account_id:number): Promise<Account> {
       return this.bankDao.getAccountById(account_id)
    }
    retrieveClientAccounts(bankclient:number): Promise<Account[]> {
        return this.bankDao.getAccountsByClientId(bankclient)
    }
    async updateClientById(bankClient: BankClient): Promise<BankClient> {
        const testAccountID:BankClient = await this.bankDao.getClientById(bankClient.bankClientId)

        if(testAccountID.bankClientId != bankClient.bankClientId){
            throw Error (`client ID's do not match`)
    }
    return this.bankDao.updateClient(bankClient)
}
    async updateAccountById(account: Account): Promise<Account> {
        const testAccountID:Account = await this.bankDao.getAccountById(account.accountId);
        const testClientID:BankClient = await this.bankDao.getClientById(account.bankClientId)

        if(testClientID.bankClientId != account.bankClientId){
            throw Error(`Client ID's do not match`)
        }else if (testAccountID.accountId != account.accountId){
            throw Error(`Account ID's do not match`)
        }
        return this.bankDao.updateAccount(account)
    }
    async withdrawAccount(account_id:number,amount:number): Promise<Account> {
        const account:Account = await this.bankDao.getAccountById(account_id)
        if(account.accountId != account_id){
            throw new MissingResourceError(`The account with ID ${account_id} does not exist`)
        }if(account.amount<amount){
            throw new Error(`You do not have enough funds to withdraw that amount`);
        } account.amount = account.amount-amount;
        await this.bankDao.modifyAccount(account,amount,1)
        return account
    }
    async depositAccount(account_id:number,amount:number,transactionType:number): Promise<Account> {
        const account:Account = await this.bankDao.getAccountById(account_id)
        return this.bankDao.modifyAccount(account,amount,0)
    }
    async deleteClientById(bank_client_id: number): Promise<boolean> {
        const bankClient:BankClient = await this.bankDao.getClientById(bank_client_id)
        if(bankClient.bankClientId != bank_client_id){
            throw new MissingResourceError(`There is no client with the ID of ${bank_client_id}`)
        }
        return this.bankDao.deleteClientById(bank_client_id)
    }
     async deleteAccountById(account_id:number): Promise<boolean> {
        const account:Account = await this.bankDao.getAccountById(account_id)
        if(account_id != account.accountId){
            throw new MissingResourceError(`There is no account with the ID of ${account_id}`)
        }
        return this.bankDao.deleteAccountById(account_id)
    }
} 