import { BankClient, Account } from "../src/entities";

export default interface ClientAccountServices{
    
    createNewAccount(account:Account,bank_Client_Id:number):Promise<Account>;

    createNewClient(bankclient:BankClient):Promise<BankClient>;

    createNewAccountByClientId(account:Account,bankClientId:number,acctType:number):Promise<Account>
    
    retrieveAllClients():Promise<BankClient[]>;

    retrieveClientById(bank_client_id:number):Promise<BankClient>;

    retrieveAccountById(account_id:number):Promise<Account>;

    retrieveClientAccounts(bankclient:number):Promise<Account[]>;

    updateClientById(bank_client_id:BankClient):Promise<BankClient>;

    updateAccountById(account_id:Account):Promise<Account>;

    withdrawAccount(account_id:number, amount:number,transactionType:number):Promise<Account>;

    depositAccount(account_id:number, amount:number, transactionType:number):Promise<Account>;

    deleteClientById(bank_client_id:number):Promise<boolean>;

    deleteAccountById(account_id:number):Promise<boolean>

}